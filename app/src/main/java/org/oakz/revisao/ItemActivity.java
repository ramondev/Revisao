package org.oakz.revisao;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import Utils.Revisoes;
import Utils.RevisoesDAO;

/**
 * Created by ramon on 22/04/16.
 */
public class ItemActivity extends AppCompatActivity {

    String keyId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivy_item);

        Intent intent = getIntent();

        keyId = intent.getStringExtra("key");

        ArrayList<Revisoes> revisao = new ArrayList<>();

        Cursor cursor = new RevisoesDAO(this).listaItem(keyId);

        String veiculo = "";
        String modeloNome = "";
        String km = "";
        String valor = "";
        String data = "";
        String local = "";
        String pecas = "";

        if(cursor.getCount() > 0){
            do {
                veiculo = cursor.getString(1);
                modeloNome = cursor.getString(2);
                km = cursor.getString(3);
                valor = cursor.getString(5);
                data = cursor.getString(6);
                local = cursor.getString(7);
                pecas = cursor.getString(4);
                revisao.add(new Revisoes(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getString(4), cursor.getDouble(5), cursor.getString(6), cursor.getString(7)));

            } while (cursor.moveToNext());
        }

        TextView itemVeiculo = (TextView) findViewById(R.id.itemVeiculo);
        TextView itemModeloNome = (TextView) findViewById(R.id.modeloNomeItem);
        TextView itemKm = (TextView) findViewById(R.id.kmItem);
        TextView itemValor = (TextView) findViewById(R.id.valorItem);
        TextView itemData = (TextView) findViewById(R.id.dataItem);
        TextView itemLocal = (TextView) findViewById(R.id.localItem);
        TextView itemPecas = (TextView) findViewById(R.id.pecasItem);

        // capturando o idioma/país configurado do sistema para setar o tipo de moeda correto à exibir
        String lingua = Locale.getDefault().getISO3Country();
        String formatado = "";

        if (lingua.equals("USA")) {
            formatado = new DecimalFormat("US$ ##,##0.00").format(Double.valueOf(valor));
        } else if (lingua.equals("BRA")) {
            formatado = new DecimalFormat("R$ ##,##0.00").format(Double.valueOf(valor));
        }

        itemVeiculo.setText(" "+veiculo);
        itemModeloNome.setText(" "+modeloNome);
        itemKm.setText(" "+km);
        itemValor.setText(" "+formatado);
        itemData.setText(" "+data);
        itemLocal.setText(" "+local);
        itemPecas.setText(" "+pecas);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_delete:

                new RevisoesDAO(this).delete(keyId);
                finish();
                Toast.makeText(ItemActivity.this, "Excluído com sucesso!", Toast.LENGTH_SHORT).show();

                break;

            case R.id.action_editar:

                Intent intent = new Intent(ItemActivity.this, EdicaoActivity.class);
                System.out.println(".............<<<<<<<<<<<<<<<<<<<<<<<<<<<<< "+keyId);
                intent.putExtra("key", keyId);
                startActivity(intent);

                break;
        }

        return super.onOptionsItemSelected(item);
    }

}