package org.oakz.revisao;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import Utils.Revisoes;
import Utils.RevisoesDAO;

/**
 * Created by ramon on 25/04/16.
 */
public class EdicaoActivity extends AppCompatActivity {

    Calendar calendario;
    EditText data;
    String veiculo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edicao);

        calendario = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendario.set(Calendar.YEAR, year);
                calendario.set(Calendar.MONTH, monthOfYear);
                calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final Intent intent = getIntent();

        final String key = intent.getStringExtra("key");

        Cursor cursor = new RevisoesDAO(this).listaItem(intent.getStringExtra("key"));

        RadioButton carro = (RadioButton) findViewById(R.id.carroEdit);
        RadioButton moto = (RadioButton) findViewById(R.id.motoEdit);
        RadioButton bike = (RadioButton) findViewById(R.id.bicicletaEdit);

        if (cursor.getString(1).equals("Carro")){
            carro.setChecked(true);
            veiculo = cursor.getString(1).toString();
        } else if (cursor.getString(1).equals("Moto")) {
            moto.setChecked(true);
            veiculo = cursor.getString(1).toString();
        } else if (cursor.getString(1).equals("Bicicleta")) {
            bike.setChecked(true);
            veiculo = cursor.getString(1).toString();
        }

        final EditText modeloNome = (EditText) findViewById(R.id.modeloNomeEdit);
        final EditText kmAtual = (EditText) findViewById(R.id.kmEdit);
        final EditText valor = (EditText) findViewById(R.id.valorEdit);
        data = (EditText) findViewById(R.id.dataEdit);
        final EditText local = (EditText) findViewById(R.id.localEdit);
        final EditText pecas = (EditText) findViewById(R.id.pecasEdit);

        modeloNome.setText(cursor.getString(2));
        kmAtual.setText(cursor.getString(3));
        valor.setText(cursor.getString(5));
        data.setText(cursor.getString(6));
        local.setText(cursor.getString(7));
        pecas.setText(cursor.getString(4));

        data.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(EdicaoActivity.this, date, calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH), calendario.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        Button salvar = (Button) findViewById(R.id.salvarEdit);

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Revisoes revisao = new Revisoes(
                        veiculo,
                        modeloNome.getText().toString(),
                        Double.parseDouble(kmAtual.getText().toString()),
                        pecas.getText().toString(),
                        Double.parseDouble(valor.getText().toString()),
                        data.getText().toString(),
                        local.getText().toString()
                );

                String text = new RevisoesDAO(getBaseContext()).editar(intent.getStringExtra("key"), revisao);
                Toast.makeText(EdicaoActivity.this, text, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(EdicaoActivity.this, ItemActivity.class);
                System.out.println("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo "+key);
                intent.putExtra("key", key);
                startActivity(intent);
            }
        });

    }

    private void updateLabel() {

        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        data.setText(sdf.format(calendario.getTime()));
    }
}
