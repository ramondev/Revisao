package org.oakz.revisao;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import Utils.Revisoes;
import Utils.RevisoesDAO;

public class AdicionarActivity extends AppCompatActivity {

    EditText data;
    Calendar calendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar);

        data = (EditText) findViewById(R.id.data);
        calendario = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendario.set(Calendar.YEAR, year);
                calendario.set(Calendar.MONTH, monthOfYear);
                calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        data.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(AdicionarActivity.this, date, calendario.get(Calendar.YEAR), calendario.get(Calendar.MONTH), calendario.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        Button salvar = (Button) findViewById(R.id.salvar);

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String veiculo = "";

                RadioButton veiculoBicicelta = (RadioButton) findViewById(R.id.bicicleta);
                RadioButton veiculoMoto = (RadioButton) findViewById(R.id.moto);
                RadioButton veiculoCarro = (RadioButton) findViewById(R.id.carro);

                if(veiculoBicicelta.isChecked()){
                    veiculo = veiculoBicicelta.getText().toString();
                } else if (veiculoMoto.isChecked()) {
                    veiculo = veiculoMoto.getText().toString();
                } else {
                    veiculo = veiculoCarro.getText().toString();
                }

                EditText modeloNome = (EditText) findViewById(R.id.modeloNome);
                EditText kmAtual = (EditText) findViewById(R.id.quilometragem);
                EditText pecas = (EditText) findViewById(R.id.pecas);
                EditText valor = (EditText) findViewById(R.id.valor);
                EditText data = (EditText) findViewById(R.id.data);
                EditText local = (EditText) findViewById(R.id.local);

                final Revisoes revisao = new Revisoes(
                        veiculo,
                        modeloNome.getText().toString(),
                        Double.parseDouble(kmAtual.getText().toString()),
                        pecas.getText().toString(),
                        Double.parseDouble(valor.getText().toString()),
                        data.getText().toString(),
                        local.getText().toString()
                );

                String text = new RevisoesDAO(getBaseContext()).adicionar(revisao);
                Toast.makeText(AdicionarActivity.this, text, Toast.LENGTH_SHORT).show();

                finish();

            }
        });

    }

    private void updateLabel() {

        String myFormat = "dd/MM/yy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        data.setText(sdf.format(calendario.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sobre) {
            Intent sobreIntent = new Intent(AdicionarActivity.this, SobreActivity.class);
            startActivity(sobreIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
