package org.oakz.revisao;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import java.util.Locale;

public class SobreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        TextView textView =(TextView)findViewById(R.id.link);
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        // capturando o idioma/país configurado do sistema para setar o texto correto
        String lingua = Locale.getDefault().getISO3Country();
        String text = "";

        if (lingua.equals("USA")) {
            text = "Launcher icon repairman by Yu luck from the <a href='https://thenounproject.com/term/repairman/311667/'>Noun Project</a>.";
        } else if (lingua.equals("BRA")) {
            text = "Ícone de lançamento do app (repairman) criado por Yu luck do <a href='https://thenounproject.com/term/repairman/311667/'> The Noun Project</a>.";
        }

        textView.setText(Html.fromHtml(text));
    }
}
