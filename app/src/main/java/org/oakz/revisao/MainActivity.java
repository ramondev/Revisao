package org.oakz.revisao;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import Utils.Revisoes;
import Utils.RevisoesAdapter;
import Utils.RevisoesDAO;

public class MainActivity extends AppCompatActivity {

    private ListView listaRevisoes;
    private List values;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(MainActivity.this, AdicionarActivity.class);
                startActivity(addIntent);
            }
        });
        // usar o array e cursor pegar o _id do sqlite : cursor.getInt(0)
        final ArrayList<Revisoes> revisoes = new ArrayList<>();

        Cursor cursor = new RevisoesDAO(getBaseContext()).listar();
        if(cursor.getCount() > 0){
            do {
                revisoes.add(new Revisoes(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getString(4), cursor.getDouble(5), cursor.getString(6), cursor.getString(7)));
            } while (cursor.moveToNext());
        }

        listaRevisoes = (ListView) findViewById(R.id.listaRevisoes);
        RevisoesAdapter adapter = new RevisoesAdapter(revisoes, this);
        listaRevisoes.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        listaRevisoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // criando um objeto do tipo Revisoes (model)
                Revisoes re = revisoes.get(position);
                // atribuindo à uma String o valor do _id retornado pelo método getId() usado pelo objeto do tipo Revisoes (model)
                String text = String.valueOf(re.getId());

                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "+re.getId());
                // passando o valor da variável para ser usada na busca do sqlite em outra activity
                Intent listaItem = new Intent(MainActivity.this, ItemActivity.class);
                listaItem.putExtra("key", text);
                startActivity(listaItem);

            }
        });

        for(Revisoes re : revisoes){
            System.out.println("O id é "+ re.getId());
            System.out.println("Do obejeto "+re.getVeiculo());
            System.out.println("-----------------");
        }

    }

    @Override
    protected void onRestart() {

        final ArrayList<Revisoes> revisoes = new ArrayList<>();

        Cursor cursor = new RevisoesDAO(getBaseContext()).listar();
        if(cursor.getCount() > 0){
            do {
                revisoes.add(new Revisoes(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getString(4), cursor.getDouble(5), cursor.getString(6), cursor.getString(7)));

            } while (cursor.moveToNext());
        }

        listaRevisoes = (ListView) findViewById(R.id.listaRevisoes);
        RevisoesAdapter adapter = new RevisoesAdapter(revisoes, this);
        listaRevisoes.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        listaRevisoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // criando um objeto do tipo Revisoes (model)
                Revisoes newre = revisoes.get(position);
                // atribuindo à uma String o valor do _id retornado pelo método getId() usado pelo objeto do tipo Revisoes (model)
                String text = String.valueOf(newre.getId());

                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx "+newre.getId());
                // passando o valor da variável para ser usada na busca do sqlite em outra activity
                Intent listaItem = new Intent(MainActivity.this, ItemActivity.class);
                listaItem.putExtra("key", text);
                startActivity(listaItem);

            }
        });

        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sobre) {
            Intent sobreIntent = new Intent(MainActivity.this, SobreActivity.class);
            startActivity(sobreIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
