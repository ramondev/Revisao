package Utils;

public class Revisoes {
    private String veiculo;
    private String modeloNome;
    private double km;
    private String pecas;
    private double valor;
    private String data;
    private String local;
    private int id;

    public Revisoes() {
    }

    public Revisoes(String veiculo, String modeloNome, double km, String pecas, double valor, String data, String local) {
        this.veiculo = veiculo;
        this.modeloNome = modeloNome;
        this.km = km;
        this.pecas = pecas;
        this.valor = valor;
        this.data = data;
        this.local = local;
    }

    public Revisoes(int id, String veiculo, String modeloNome, double km, String pecas, double valor, String data, String local) {
        this.id = id;
        this.veiculo = veiculo;
        this.modeloNome = modeloNome;
        this.km = km;
        this.pecas = pecas;
        this.valor = valor;
        this.data = data;
        this.local = local;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public String getModeloNome() {
        return modeloNome;
    }

    public void setModeloNome(String modeloNome) {
        this.modeloNome = modeloNome;
    }

    public double getKm() {
        return km;
    }

    public void setKm(double km) {
        this.km = km;
    }

    public String getPecas() {
        return pecas;
    }

    public void setPecas(String pecas) {
        this.pecas = pecas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
