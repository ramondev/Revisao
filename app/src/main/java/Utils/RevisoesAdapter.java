package Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.oakz.revisao.R;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class RevisoesAdapter extends BaseAdapter {

    String veiculo = "";

    private ArrayList<Revisoes> revisoes;
    Context context;

    public RevisoesAdapter(ArrayList<Revisoes> revisoes, Context context) {
        this.revisoes = revisoes;
        this.context = context;
    }

    @Override
    public int getCount() {
        return revisoes.size();
    }

    @Override
    public Object getItem(int position) {
        return revisoes.get(position);
    }

    @Override
    public long getItemId(int position) {
        //return revisoes.get(position).getId();
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Revisoes revisao = revisoes.get(position);

        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View item = inflater.inflate(R.layout.itens_revisoes, null);

        // setando a imagem que aparece ao lado das revisões na MainActivity
        ImageView img = (ImageView) item.findViewById(R.id.imgVeiculo);

        veiculo = revisao.getVeiculo();

        if (veiculo.equals("Bicicleta")) {
            img.setImageResource(R.drawable.ic_bike);
        } else if (veiculo.equals("Moto")) {
            img.setImageResource(R.drawable.ic_motorcycle);
        } else if (veiculo.equals("Carro")) {
            img.setImageResource(R.drawable.ic_car);
        }

        // fim da alteração das imagens
        TextView itemData = (TextView) item.findViewById(R.id.itemData);
        TextView itemModeloNome = (TextView) item.findViewById(R.id.itemModeloNome);
        TextView itemLocal = (TextView) item.findViewById(R.id.itemLugar);
        TextView itemValor = (TextView) item.findViewById(R.id.itemValor);

        itemData.setText(revisao.getData());
        itemModeloNome.setText(revisao.getModeloNome());
        itemLocal.setText(revisao.getLocal());

        // capturando o idioma/país configurado do sistema para setar o tipo de moeda correto à exibir
        String lingua = Locale.getDefault().getISO3Country();
        String formatado = "";

        if (lingua.equals("USA")) {
            formatado = new DecimalFormat("US$ ##,##0.00").format(revisao.getValor());
        } else if (lingua.equals("BRA")) {
            formatado = new DecimalFormat("R$ ##,##0.00").format(revisao.getValor());
        }

        itemValor.setText(formatado);

        // fim da configuração do tipo de moeda à ser exibido

        return item;
    }
}
