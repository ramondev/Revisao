package Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class RevisoesDAO {

    private SQLiteDatabase db;
    private Context context;

    public RevisoesDAO(Context context) {
        this.context = context;
    }

    public String adicionar(Revisoes revisao){

        db = new DAO(context).getWritableDatabase();

        ContentValues valores = new ContentValues();

        valores.put("VEICULO", revisao.getVeiculo()); // 1
        valores.put("MODELO", revisao.getModeloNome()); // 2
        valores.put("KMATUAL", revisao.getKm()); // 3
        valores.put("PECAS", revisao.getPecas()); // 4
        valores.put("VALOR", revisao.getValor()); // 5
        valores.put("DATA", revisao.getData()); // 6
        valores.put("LOCAL", revisao.getLocal()); // 7

        long result = db.insert(DAO.TABLE, null, valores);
        String retorno;
        if(result != -1){
            retorno = "Revisão salva.";
        } else {
            retorno = "Não foi possível salvar esta revisão!";
        }

        db.close();

        return retorno;
    }

    public Cursor listar(){

        db = new DAO(context).getReadableDatabase();

        String[] args = {};
        Cursor cursor = db.rawQuery("select * from Revisoes", args);
        if (cursor != null){
            cursor.moveToFirst();
        }

        db.close();

        return cursor;
    }

    public Cursor listaItem(String id){

        db = new DAO(context).getReadableDatabase();

        String ID = id;
        String[] args = {};
        Cursor cursor = db.rawQuery("select * from Revisoes where _id = "+ID, args);
        if(cursor != null){
            cursor.moveToFirst();
        }

        db.close();

        return cursor;
    }

    public void delete(String id) {

        db = new DAO(context).getWritableDatabase();


        String ID = id;
        String[] args = {};
        Cursor cursor = db.rawQuery("delete from Revisoes where _id = "+ID, args);
        if(cursor != null){
            cursor.moveToFirst();
        }

        db.close();
    }

    public String editar(String id, Revisoes revisao) {

        db = new DAO(context).getWritableDatabase();

        ContentValues valores = new ContentValues();

        valores.put("VEICULO", revisao.getVeiculo()); // 1
        valores.put("MODELO", revisao.getModeloNome()); // 2
        valores.put("KMATUAL", revisao.getKm()); // 3
        valores.put("PECAS", revisao.getPecas()); // 4
        valores.put("VALOR", revisao.getValor()); // 5
        valores.put("DATA", revisao.getData()); // 6
        valores.put("LOCAL", revisao.getLocal()); // 7

        String retorno;
        String ID = id;
        String[] args = {};
        long result = db.update("Revisoes", valores, "_id = " + id, null);
        //Cursor cursor = db.rawQuery("update Revisoes where _id = "+ID, args);
        if(result != -1){
            retorno = "Revisão editada com sucesso.";
        } else {
            retorno = "Não foi possível editar esta revisão!";
        }

        db.close();

        return retorno;
    }
}
