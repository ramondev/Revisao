package Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DAO extends SQLiteOpenHelper {

    private static final String NOME_BANCO = "database.db";
    protected static final String TABLE = "Revisoes";
    private static final int versao = 1;

    public DAO(Context context) {
        super(context, NOME_BANCO, null, versao);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, VEICULO TEXT NOT NULL, " +
                "MODELO TEXT NOT NULL, KMATUAL FLOAT NOT NULL, PECAS TEXT NOT NULL, VALOR FLOAT NOT NULL, " +
                "DATA TEXT NOT NULL, LOCAL TEXT NOT NULL)";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, VEICULO TEXT NOT NULL, " +
                "MODELO TEXT NOT NULL, KMATUAL FLOAT NOT NULL, PECAS TEXT NOT NULL, VALOR FLOAT NOT NULL, DATA TEXT NOT NULL," +
                " LOCAL TEXT NOT NULL)";

        db.execSQL(sql);

    }
}
